module JSON

  def self.parse jsonString
    jsonData = jsonString.dataUsingEncoding NSUTF8StringEncoding
    error = Pointer.new(:object)                                     
    planData = NSJSONSerialization.JSONObjectWithData(jsonData, options:0, error:error)  
    if not planData
      raise error[0] if error[0]
    end
    planData
  end

  def self.generate obj, pretty=false 
    options = pretty ? NSJSONWritingPrettyPrinted : 0
    error = Pointer.new(:object)  
    data = NSJSONSerialization.dataWithJSONObject(obj, options:options, error:error)
    if not data 
      raise error[0] if error[0]
    end
    NSString.alloc.initWithData data, encoding:NSUTF8StringEncoding
  end

end