module App 

  def self.alert message, &block
    alert = UIAlertView.alloc.init 
    alert.setTitle message
    yield(alert) if block_given?
    alert.addButtonWithTitle "Ok" if alert.numberOfButtons == 0
    alert.show 
    alert 
  end

  def self.delegate 
    UIApplication.sharedApplication.delegate
  end

  def self.docsPath
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0]
  end

  def self.tempPath 
    NSTemporaryDirectory()
  end

  def self.resourcesPath
    NSBundle.mainBundle.resourcePath
  end


  def self.createUUID 
    NSUUID.UUID.UUIDString
  end

end

