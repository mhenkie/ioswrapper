module Device 

  def self.isIpad  
    UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad
  end

  def self.orientation 
  	UIApplication.sharedApplication.statusBarOrientation
  end

  def self.simulator?
    @simulator_state ||= !(UIDevice.currentDevice.model =~ /simulator/i).nil?
  end

end
