module Store 

  def self.managedObjectContext 
    return @managedObjectContext if @managedObjectContext
  
    coordinator = self.persistentStoreCoordinator
    if coordinator != nil 
      @managedObjectContext = NSManagedObjectContext.alloc.init
      @managedObjectContext.setPersistentStoreCoordinator coordinator
    end
     
    return @managedObjectContext
  end
   
   
  def self.managedObjectModel 
    return @managedObjectModel if @managedObjectModel
  
    @managedObjectModel = NSManagedObjectModel.mergedModelFromBundles nil
     
    return @managedObjectModel
  end


  def self.persistentStoreCoordinator 
    return @persistentStoreCoordinator if @persistentStoreCoordinator

    docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0]
    storeUrl = NSURL.fileURLWithPath docsPath.stringByAppendingPathComponent "db.sqlite"
    error    = Pointer.new(:object)
    @persistentStoreCoordinator = NSPersistentStoreCoordinator.alloc.initWithManagedObjectModel self.managedObjectModel

    options = { NSMigratePersistentStoresAutomaticallyOption => true, NSInferMappingModelAutomaticallyOption => true }

    if !@persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration:nil, URL:storeUrl, options:options,error:error) 
      raise error[0].localizedDescription if error[0]
    end
    
    return @persistentStoreCoordinator
  end

  def self.save error=nil
    managedObjectContext.save error
  end

  def self.delete object 
    managedObjectContext.deleteObject object
  end

end