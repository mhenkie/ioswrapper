class NSManagedObject

  def inspect
    properties = []
    entity.properties.select { |p| p.is_a?(NSAttributeDescription) }.each do |property|
      properties << "#{property.name}: #{valueForKey(property.name).inspect}"
    end

    "#<#{entity.name} #{properties.join(", ")}>"
  end

  def self.new 
    alloc.initWithEntity(entity, insertIntoManagedObjectContext:Store.managedObjectContext)
  end

  def self.entity
    @entity ||= Store.managedObjectModel.entitiesByName[name]
  end

  def self.all 
    fetcher.all
  end

  def self.first 
    fetcher.first
  end

  def self.last
    fetcher.last
  end

  def self.limit(l)
    fetcher.limit(l)
  end

  def self.offset(o)
    fetcher.offset(o)
  end 

  def self.order(*args)
    fetcher.order(*args)
  end

  def self.where(*args)
    fetcher.where(*args)
  end  

  def self.fetcher
    fetchRequest = Fetcher.alloc.init
    fetchRequest.setEntity entity
    fetchRequest
  end

  def self.count 
    request = NSFetchRequest.alloc.init
    request.setEntity entity
    request.setIncludesSubentities false # Omit subentities. Default is YES (i.e. include subentities)

    error = Pointer.new(:object)
    count = Store.managedObjectContext.countForFetchRequest request, error:error
    if count == NSNotFound 
      raise error[0] if error[0]
    end
    return count
  end

  def self.truncate
    request = NSFetchRequest.alloc.init
    request.setEntity entity
    request.setIncludesPropertyValues false 
    request.setIncludesSubentities false

    error = Pointer.new(:object)
    all_objects = Store.managedObjectContext.executeFetchRequest request, error:error
   
    all_objects.each do |evaluation|
      Store.managedObjectContext.deleteObject evaluation
    end if all_objects
   
    saveError = Pointer.new(:object)
    Store.managedObjectContext.save saveError
  end

end